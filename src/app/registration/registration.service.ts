import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { SettingService } from '../setting.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient, private settingService: SettingService) {}

  registration(name: any, email: any, password: any, role: any, city: any): Observable < any[] > {
    return this.http.post < any[] > (this.settingService.url + '/api/registration', {
      'name': name,
      'email': email,
      'password': password,
      'role': role,
      'city': city
    })
  }
}
