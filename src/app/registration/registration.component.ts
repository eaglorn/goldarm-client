import { Component, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { DataService } from '../data.service'
import { RegistrationService } from './registration.service'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

export interface Role {
    value: string
}

export interface City {
    value: string
}

export interface Regions {
    parent?: string
    name: string
    children?: Regions[]
}

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

    cityChecked = ''
  
    passwordHide: any = true

    roles: Role[] = [
        {
            value: 'Заказчик'
        },
        {
            value: 'Работник'
        },
    ]

    cities: City[] = [
        {
            value: 'Хабаровск'
        },
        {
            value: 'Большой камень'
        },
        {
            value: 'Владивосток'
        },
    ]

    TREE_DATA: Regions[] = [
        {
            name: 'Дальневосточный Федеральный округ',
            children: [
                {
                    name: 'Хабаровский край',
                    children: [
                        {
                            parent: 'Дальневосточный Федеральный округ, Хабаровский край, ',
                            name: 'Хабаровск'
                        },
                        {
                            parent: 'Дальневосточный Федеральный округ, Хабаровский край, ',
                            name: 'Комсомольск-на-Амуре'
                        },
                    ]
                }, {
                name: 'Приморский край',
                    children: [
                        {
                            parent: 'Дальневосточный Федеральный округ, Приморский край, ',
                            name: 'Владивосток'
                        },
                        {
                            parent: 'Дальневосточный Федеральный округ, Приморский край, ',
                            name: 'Уссурийск'
                        },
                    ]
                },
            ]
        },
    ]

    loginFormControl = new FormControl('', [
        Validators.required
    ])
    
    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email
    ])
    
    passwordFormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(12),
        Validators.pattern(/^[A-z0-9]*$/)
    ])

    roleFormControl = new FormControl('', [
        Validators.required
    ])

    treeControl = new NestedTreeControl<Regions>(node => node.children);
    dataSource = new MatTreeNestedDataSource<Regions>();

    constructor(private router: Router, private dataService: DataService, private registrationService: RegistrationService, private toastr: ToastrService) {
        this.dataSource.data = this.TREE_DATA;
    }

    hasChild = (_: number, node: Regions) => !!node.children && node.children.length > 0;

    registration() {
        this.registrationService.registration(this.loginFormControl.value, this.emailFormControl.value, this.passwordFormControl.value, this.roleFormControl.value, this.cityChecked).subscribe(result => {
            const value: any = result
            if (value.email === false) {
                this.toastr.warning('Под данным email уже зарегистрирован пользователь');
            } else {
                this.toastr.success('Вы успешно зарегистрировались.');
                this.dataService.auth = true
                this.dataService.name = this.loginFormControl.value
                this.dataService.password = this.passwordFormControl.value
                this.dataService.email = this.emailFormControl.value
                this.dataService.role = this.roleFormControl.value
                this.dataService.city = this.cityChecked
                this.dataService.id = value.id
                this.dataService.type = 'user'
                this.dataService.image = '0'
                this.router.navigate(['/profile']);
            }
        })
    }

    ngOnInit() { }
}
