import { Component,OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { DataService } from '../data.service'
import { AuthService } from '../auth/auth.service'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit {
  
  passwordHide: any = true

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ])
  
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.maxLength(12),
    Validators.pattern(/^[A-z0-9]*$/)
  ])

  constructor(private dataService: DataService, private authService: AuthService, private toastr: ToastrService, public router: Router) {}

  auth() {
    this.authService.get(this.emailFormControl.value, this.passwordFormControl.value).subscribe(result => {
      const value: any = result
      if (value.email === false) {
        this.toastr.warning('Введёный email не зарегистрирован', ' ');
      } else {
        if (value.password === false) {
          this.toastr.warning('Неправильно введён пароль', ' ');
        } else {
          this.dataService.auth = true
          this.dataService.email = value.email
          this.dataService.name = value.name
          this.dataService.password = this.passwordFormControl.value
          this.dataService.type = value.type
          this.dataService.image = value.image
          this.dataService.city = value.city
          this.dataService.role = value.role
          this.toastr.success('Вы успешно авторизировались', ' ');
          this.router.navigate(['/profile']);
        }
      }
    })
  }

  ngOnInit() {}

}
