import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { SettingService } from '../setting.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private settingService: SettingService) {}

  get(email: any, password: any): Observable < any[] > {
    return this.http.post < any[] > (this.settingService.url + '/api/authorization', {
      'email': email,
      'password': password
    })
  }
}
