import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { SettingService } from '../setting.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient, private settingService: SettingService) {}

  sendProfileImage(image: FormData): Observable < any[] > {
    return this.http.post < any[] > (this.settingService.url + '/api/profile_upload_image', image)
  }
}
