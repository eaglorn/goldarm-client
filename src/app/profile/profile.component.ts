import { Component, OnInit } from '@angular/core'
import { ProfileService } from './profile.service'
import { DataService } from '../data.service'
import { ToastrService } from 'ngx-toastr'
import { SettingService } from '../setting.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  constructor(private profileService: ProfileService, public dataService: DataService, private toastr: ToastrService, public settingService: SettingService) { }

  onFileChange(event) {
    var file : File = event.target.files[0]
    if(file.size > 1024 * 1024 * 5) {
      this.toastr.warning('Изображение превышает 5 МБ');
    } else {
      const formData = new FormData();
      formData.append('email', this.dataService.email);
      formData.append('password', this.dataService.password);
      formData.append('image', file);
      this.profileService.sendProfileImage(formData).subscribe(result =>{
        this.toastr.warning('Изображение профиля изменено');
        const value: any = result
        this.dataService.image = value.name
      })
    }
  }

  ngOnInit () { }

}
