import { Injectable } from '@angular/core'
import { SessionStorage } from 'ngx-store'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  @SessionStorage({key: 'auth'}) auth = false

  @SessionStorage({key: 'email'}) email = ''

  @SessionStorage({key: 'name'}) name = ''

  @SessionStorage({key: 'password'}) password = ''

  @SessionStorage({key: 'type'}) type = ''

  @SessionStorage({key: 'role'}) role = ''

  @SessionStorage({key: 'city'}) city = ''

  @SessionStorage({key: 'id'}) id = 0

  @SessionStorage({key: 'image'}) image = ''

  clean () {
    this.auth = false
    this.email = ''
    this.name = ''
    this.password = ''
    this.type = ''
    this.role = ''
    this.city = ''
    this.id = 0
    this.image = ''
  }

}
