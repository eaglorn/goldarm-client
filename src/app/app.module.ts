import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { MatToolbarModule } from '@angular/material/toolbar'
import { MatMenuModule } from '@angular/material/menu'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatTabsModule } from '@angular/material/tabs'
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatSnackBar, MatSnackBarModule} from '@angular/material'
import { MatInputModule } from '@angular/material'
import { MatStepperModule } from '@angular/material/stepper'
import { MatSelectModule } from '@angular/material/select'
import { MatListModule } from '@angular/material/list'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatTreeModule } from '@angular/material/tree'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatRadioModule } from '@angular/material/radio'

import { AgGridModule } from 'ag-grid-angular'

import { NgSelectModule } from '@ng-select/ng-select'

import { FilePickerModule } from  'ngx-awesome-uploader'

import { ToastrModule } from 'ngx-toastr'

import { AppComponent } from './app.component'

import { AppRoutingModule } from './app-routing.module'

import { AuthComponent } from './auth/auth.component'
import { AuthService } from './auth/auth.service'

import { RegistrationComponent } from './registration/registration.component'
import { RegistrationService } from './registration/registration.service'

import { HomeComponent } from './home/home.component'

import { ProfileComponent } from './profile/profile.component'

import { OrderCreateComponent } from './order/order-create/order-create.component'

import { SettingService } from './setting.service'
import { DataService } from './data.service'

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HomeComponent,
    RegistrationComponent,
    ProfileComponent,
    OrderCreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatSnackBarModule,
    MatStepperModule,
    MatSelectModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true,
      countDuplicates: true,
      resetTimeoutOnDuplicate: true
    }),
    MatListModule,
    MatGridListModule,
    NgSelectModule,
    FilePickerModule,
    MatTreeModule,
    MatCheckboxModule,
    MatRadioModule,
    AgGridModule.withComponents([]),
  ],
  providers: [
    AuthService,
    RegistrationService,
    DataService,
    MatSnackBar,
    SettingService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
