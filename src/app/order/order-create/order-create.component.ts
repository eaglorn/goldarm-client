import { Component, OnInit, ViewChild } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { HttpClient } from  '@angular/common/http';
import { ImageFilePickerAdapter } from  './file-picker.adapter';
import { FilePickerComponent } from 'ngx-awesome-uploader'
import { ValidationError } from 'ngx-awesome-uploader'
import { FilePreviewModel } from 'ngx-awesome-uploader'
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.sass']
})
export class OrderCreateComponent implements OnInit {

  titleFormControl = new FormControl('', [
    Validators.required,
  ])

  descriptionFormControl = new FormControl('', [
    Validators.required,
  ])

  summFormControl = new FormControl('', [
    Validators.required,
  ])

  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new ImageFilePickerAdapter(this.http);
  myFiles: FilePreviewModel[] = [];
  constructor(private http: HttpClient) { }

  ngOnInit() {}
  onValidationError(e: ValidationError) { console.log(e) }
  onUploadSuccess(e: FilePreviewModel) { }
  onRemoveSuccess(e: FilePreviewModel) { console.log(e) }
  onFileAdded(file: FilePreviewModel) { this.myFiles.push(file)}
  removeFile() { this.uploader.removeFileFromList(this.myFiles[0].fileName) }
  myCustomValidator(file: File): Observable<boolean> {
    console.log(file.name.length);
    if (!file.name.includes('uploader')) {
      return  of(true).pipe(delay(2000));
    }
    if (file.size > 50) {
      return this.http.get('https://vugar.free.beeceptor.com').pipe(map((res) =>  res === 'OK' ));
    }
    return of(false).pipe(delay(2000));
  }
}
